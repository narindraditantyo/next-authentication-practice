import {
  Button,
  Card,
  CardTitle,
  Container,
} from "reactstrap";

const Logout = () => {
  return (
    <Container className="vh-100 py-5">
      <Card body className="mx-auto w-50 p-5">
        <CardTitle tag="h2" className="text-center mb-5">
          You're logged in!
        </CardTitle>
        {/* Put user data inside div */}
        <div></div>
        <Button type="button" color="danger" className="w-100 mt-4" onClick={() => {}}>Logout</Button>
      </Card>
    </Container>
  );
};

export default Logout;
