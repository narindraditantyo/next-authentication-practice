import { Button, Card, CardTitle, Container, Form, FormGroup, Input, Label } from "reactstrap";

const Login = () => {
  return (
    <Container className="vh-100 py-5">
      <Card body className="mx-auto w-50 p-5">
        <CardTitle tag="h2" className="text-center mb-5">Login</CardTitle>
        <Form onSubmit={() => {}}>
          <FormGroup>
            <Label for="emailInput">Email</Label>
            <Input
              id="emailInput"
              name="email"
              placeholder="Email"
              type="email"
            />
          </FormGroup>
          <FormGroup>
            <Label for="passwordInput">Password</Label>
            <Input
              id="passwordInput"
              name="password"
              placeholder="Password"
              type="password"
            />
          </FormGroup>
          <Button type="submit" className="w-100 mt-4">Login</Button>
        </Form>
      </Card>
    </Container>
  );
};

export default Login;
